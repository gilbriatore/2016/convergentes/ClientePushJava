package br.edu.up;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

public class ClienteJava {

  public static void main(String[] args) throws Exception {

    JSONObject notification = new JSONObject();
    notification.put("title", "Java");
    notification.put("body", "Notificação do Java");
    
    JSONObject message = new JSONObject();
    message.put("to", "fq6mS62kCFw:APA91bHLt86eTmu-hVOtJQt2P-689T5XsYioqGcLfKDDilLnSedT7Y_gyEkBp6HrsnOswFf7oEvv12cgIbAG6--cyW72Z-v6f84NnI4h_B7Dy5UTKepP0Ib1iJ8DWMoOQv3l8-wEokmD");
    message.put("priority", "high");
    message.put("notification", notification);
    
    String url = "https://fcm.googleapis.com/fcm/send";
    HttpPost post = new HttpPost(url);
    post.setHeader("Content-type", "application/json");
    post.setHeader("Authorization", "key=AIzaSyCvU37y3L1Cgab2zdKL2P8dnUMt8RfNQfY");
    post.setEntity(new StringEntity(message.toString(), "UTF-8"));
    
    HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response = client.execute(post);
    
    System.out.println(response);

  }
}